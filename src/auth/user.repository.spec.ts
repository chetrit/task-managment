import { UserRepository } from './user.repository';
import { Test } from "@nestjs/testing";
import { ConflictException, InternalServerErrorException } from '@nestjs/common';
import { User } from './user.entity';
import * as bcrypt from 'bcrypt';

const mockCredentialsDto = { username: 'TestUsername', password: '12345' };

describe('UserRepository', () => {
    let userRepository;

    beforeAll(async () => {
        const module = await Test.createTestingModule({
            providers: [
                UserRepository
            ]

        }).compile();

        userRepository = await module.get<UserRepository>(UserRepository);
    });

    describe('signUp', () => {
        let save;
        beforeEach(() => {
            save = jest.fn();
            userRepository.create = jest.fn().mockReturnValue({ save })
        })
        it('successfully signs up the user', async () => {
            save.mockResolvedValue(undefined);
            expect(userRepository.signUp(mockCredentialsDto)).resolves.not.toThrow();
        })

        it('throws a conflict exception a username already exists', () => {
            save.mockRejectedValue({ code: '23505' });
            expect(userRepository.signUp(mockCredentialsDto)).rejects.toThrow(ConflictException);
        })
        it('throws a conflict exception a username already exists', () => {
            save.mockRejectedValue({ code: '12321' });
            expect(userRepository.signUp(mockCredentialsDto)).rejects.toThrow(InternalServerErrorException);
        })
    });

    describe('validateUserPassword', () => {
        let user;

        beforeEach(() => {
            userRepository.findOne = jest.fn();

            user = new User()
            user.username = 'TestUsername';
            user.validatePassword = jest.fn();
        })

        it('return the username as validation successful', async () => {
            userRepository.findOne.mockResolvedValue(user);
            user.validatePassword.mockResolvedValue(true);

            const result = await userRepository.validateUserPassword(mockCredentialsDto);
            expect(result).toEqual('TestUsername')
        })

        it('return null as user cannot be found', async () => {
            userRepository.findOne.mockResolvedValue(null)
            const result = await userRepository.validateUserPassword(mockCredentialsDto);
            expect(user.validatePassword).not.toHaveBeenCalled();
            expect(result).toBeNull();
        })

        it('return null as password is invalid', async () => {
            userRepository.findOne.mockResolvedValue(user);
            user.validatePassword.mockResolvedValue(false);
            const result = await userRepository.validateUserPassword(mockCredentialsDto);
            expect(user.validatePassword).toHaveBeenCalled()
            expect(result).toBeNull()
        })
    })

    describe('hasePassword', () => {
        it('calls bycrypt.hash to generate a hash', async () => {
            bcrypt.hash = jest.fn().mockResolvedValue('teshHash');
            expect(bcrypt.hash).not.toHaveBeenCalled();
            const result = await userRepository.hashPassword('testPassword', 'testSalt');
            expect(bcrypt.hash).toHaveBeenCalledWith('testPassword', 'testSalt');
            expect(result).toEqual('teshHash');
        })
    })
});