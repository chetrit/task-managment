import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn, Unique } from "typeorm";
import * as bcrypt from 'bcrypt';
import { Task } from "../tasks/task.entity";
@Entity()
@Unique(['username'])
export class User extends BaseEntity {
    constructor(username?: string, password?: string, salt?: string) {
        super();
        this.username = username;
        this.password = password;
        this.salt = salt;
    }

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    username: string;

    @Column()
    password: string

    @Column()
    salt: string;

    @OneToMany(type => Task, (task) => task.user, { eager: false })
    tasks: Task[];

    async validatePassword(password: string): Promise<boolean> {
        const hash = await bcrypt.hash(password, this.salt);
        return hash === this.password;
    }
}