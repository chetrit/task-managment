import { GetTaskFilterDto } from './dto/get-task-filter.dto';
import { Test } from '@nestjs/testing';
import { TaskRepository } from './task.repository';
import { TasksService } from './tasks.service';
import { TaskStatus } from './task-status.enum';
import { NotFoundException } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';

const mockUser = { username: 'Test user', id: 12 };

const mockTaskRepository = () => ({
    getTasks: jest.fn(),
    CreateTask: jest.fn(),
    findOne: jest.fn(),
    delete: jest.fn()
})
describe('TaskService', () => {
    let taskService;
    let taskRepository;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            providers: [
                TasksService,
                {
                    provide: TaskRepository,
                    useFactory: mockTaskRepository
                }
            ]
        }).compile();

        taskService = await module.get<TasksService>(TasksService);
        taskRepository = await module.get<TaskRepository>(TaskRepository);

    });

    describe('getTasks', () => {
        it('gets all tasks from the repository', async () => {
            taskRepository.getTasks.mockResolvedValue('some value');

            expect(taskRepository.getTasks).not.toHaveBeenCalled();
            const filters: GetTaskFilterDto = { status: TaskStatus.IN_PROGRESS, search: 'Some search query' };
            // call taskService.getTasks
            const result = await taskService.getTasks(filters, mockUser);
            expect(taskRepository.getTasks).toHaveBeenCalled();
            expect(result).toEqual('some value');

        })
    })

    describe('getTaskById', () => {
        it('calls taskRepository.findOne() and successfuly retrive and return the task', async () => {
            const mockTask = { title: 'Test task', description: 'Test desc' }
            taskRepository.findOne.mockResolvedValue(mockTask);


            const result = await taskService.getTaskByID(1, mockUser);
            expect(result).toEqual(mockTask);

            expect(taskRepository.findOne).toHaveBeenCalledWith(
                {
                    id: 1,
                    userId: mockUser.id
                }
            )
        });
        it('throws an error as task is not found', () => {
            taskRepository.findOne.mockResolvedValue(null);
            expect(taskService.getTaskByID(1, mockUser)).rejects.toThrow(NotFoundException);

        });
    });

    describe('createTask', () => {
        it('calls taskRepository.create() and returns the result', async () => {
            taskRepository.CreateTask.mockResolvedValue('some value');
            expect(taskRepository.CreateTask).not.toHaveBeenCalled();

            const createTaskDto: CreateTaskDto = { title: 'testTask', description: 'Some search query' };
            const result = await taskService.createTask(createTaskDto, mockUser);
            expect(taskRepository.CreateTask).toHaveBeenCalledWith(createTaskDto, mockUser);
            expect(result).toEqual('some value');
        })
    })

    describe('deleteTask', () => {
        it('calls taskRepository.deleteTask() to delete a task', async () => {
            taskRepository.delete.mockResolvedValue({ affected: 1 });
            expect(taskRepository.delete).not.toHaveBeenCalled();

            await taskService.deleteTask(1, mockUser);
            expect(taskRepository.delete).toHaveBeenCalledWith({ id: 1, userId: mockUser.id })
        })

        it('throws an error as task could not be found', async () => {
            taskRepository.delete.mockResolvedValue({ affected: 0 });
            expect(taskService.deleteTask(1, mockUser)).rejects.toThrow(NotFoundException)

        })
    })

    describe('updateTaskStatus', () => {
        it('update task status', async () => {
            const save = jest.fn().mockResolvedValue(true);
            taskService.getTaskByID = jest.fn().mockResolvedValue({
                status: TaskStatus.OPEN,
                save
            });

            expect(taskService.getTaskByID).not.toHaveBeenCalled();
            const result = await taskService.updateTaskStatus(1, TaskStatus.DONE, mockUser);
            expect(taskService.getTaskByID).toHaveBeenCalled();
            expect(save).toHaveBeenCalled();
            expect(result.status).toEqual(TaskStatus.DONE);
        })
    })
})
