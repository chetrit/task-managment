class FriendsList {
    friends = [];

    addFriend(name) {
        this.friends.push(name);
        this.announceFriendship(name);
    };

    announceFriendship(name) {
        global.console.log(`${name} is now a firend`);
    }

    removeFriend(name) {
        const idx = this.friends.indexOf(name);

        if (idx === -1) {
            throw new Error('Friend not found!');
        }

        this.friends.splice(idx, 1);

    }
}

describe('FriendsList', () => {
    let friendsList;

    beforeEach(() => {
        friendsList = new FriendsList();
    });
    it('initalizes friends list', () => {
        expect(friendsList.friends.length).toEqual(0);
    });

    it('adds a friend to the list', () => {
        expect(friendsList.friends.length).toEqual(0);
        friendsList.addFriend('Ariel');
        expect(friendsList.friends.length).toEqual(1);
    })

    it('announces friendship', () => {
        friendsList.announceFriendship = jest.fn()
        expect(friendsList.announceFriendship).not.toHaveBeenCalled();
        friendsList.addFriend('Ariel');
        expect(friendsList.announceFriendship).toHaveBeenCalledWith('Ariel');
    })

    describe('removeFriends', () => {
        it('removes a friend from the list', () => {
            friendsList.addFriend('Ariel');
            expect(friendsList.friends[0]).toEqual('Ariel');
            friendsList.removeFriend('Ariel');
            expect(friendsList.friends[0]).toBeUndefined()
        });

        it('throws an error as friends does not exists', () => {
            expect(() => friendsList.removeFriend('Ariel')).toThrow(new Error('Friend not found!'));

        });
    });


});